# Exercice

## Consigne

Le but est de tester vos compétences en développement Front-End via un simple exercice.  
Cela consiste à créer une application qui affichera un gestionnaire d'articles de blog.

### Fonctionnalitées

- Afficher une liste d'articles sous forme d'accordéon.
- Avoir la possibilité de supprimer un article
- Avoir la possibilité d'éditer un article
- Avoir la possibilité de créer un article

![Wololo](gif-posts.gif)

### Les outils à utiliser

Vous devrez obligatoirement utiliser ces 3 outils:

- `Vue.js`
- `vue-resource`
- `vue-router`

Vous trouverez tout ce dont vous avez besoin dans la documentation de chaque outil.    
Il vous faudra utiliser un compiler de type `webpack` ou `parcel`.

## Ressources

Base URL: [https://jsonplaceholder.typicode.com](https://jsonplaceholder.typicode.com)

| Method        | URL                                                                        |
| :------------ | :------------------------------------------------------------------------- |
| GET           | [/posts](https://jsonplaceholder.typicode.com/posts)                       |
| GET           | [/posts/1](https://jsonplaceholder.typicode.com/posts/1)                   |
| POST          | /posts                                                                     |
| PUT           | /posts/1                                                                   |
| DELETE        | /posts/1                                                                   |

## Notes

- Le projet sera versionné avec `git`.  
- Forkez ce projet et placer votre code dans le dossier`sources`.  
- Vous pouvez utiliser Github, GitLab ou Bitbucket pour le partager.

### Quelques conseils:

- `++` si vous utilisez un stage manager du type `vuex`
- `++` si vous evitez le framework `jQuery`
- `++` si vous utilisez la syntaxe `SCSS` ou `SASS`
- `++` si vous evitez les framework du type Foundation/Bootstrap

## Compétences testées

- Manipuler un framework Front-End moderne
- Maîtrise du `HTML5`, `CSS3`, `JavaScript`
- Utilisation de `git`, `npm`...
- Utilisation d'un compiler

## Let's go !  
  
![Wololo](gif-cat.gif)